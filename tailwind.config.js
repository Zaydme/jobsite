module.exports = {
  theme: {
    extend: {
      colors: {
        lightmint: "#81E6D9",
        mint: "#319795",
        sky: "#3182CE",
        lightblue: "#E6FFFA",
        lighterblue: "#EBF4FF",
        darkgrey: "#2D3748",
        sand: "#707070",
        darkertext: "#4A5568",
        darktext: "#718096",
        lightergrey: "#CBD5E0"
      },
      boxShadow: {
        "b-md": "0px 3px 6px #00000029",
        "t-md":
          "0 -10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)"
      },
      maxWidth: {
        '14': '14rem',
        '1/2': '50%',
       }
    }
  }
};
